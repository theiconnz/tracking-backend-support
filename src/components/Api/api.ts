/*!
 * app
 * Copyright(c) 2022 KOTAFACTORY
 * Copyright(c) 2022 Don Nuwinda Udugala
 * CC Licensed
 */
import express from "express";
import ValidateInput from "../ValidateInput";
import LoadDrivers from "../Drivers"

class Api{
    private app: any;
    private server: any;
    private validator = new ValidateInput();
    private drivers : any;

    constructor() {
        this.app = express();
        this.drivers = new LoadDrivers(this.app);
        this.apiListenDefault();
        this.apiListenAddress();
        this.apiListenPort();
    }

    /*
     Listen to /api route
     */
    apiListenAddress(){
        this.app.get('/api/:key/address/:add', (req:any, res:any) => {
            let apikey = req.params.key;
            let address = req.params.add;
            let target = apikey;

            if(this.validator.validateApiKey(apikey) && this.validator.validateAddress(address) ){
                res.status(200).json({status:200, message: 'Valid key and address'})
            } else {
                res.status(400).json({status:400, message: this.validator.getFormattedMessage()})
            }
        })
    }

    /*
     Any routes shouldn't allow to run
     */
    apiListenDefault(){
        this.app.get('/', (req:any, res:any) => {
            res.status(400).json({ status:400, message:'Error page' });
        })
    }

    /*
     API route is Listen to port
     */
    apiListenPort(){
        try{
            this.server = this.app.listen(process.env.EXPRESS_PORT, function () {});
        } catch (e:any) {
            throw new Error(e);
        }
    }

}

export default Api;