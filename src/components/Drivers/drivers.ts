/*!
 * mysql drivers
 * Copyright(c) 2022 KOTAFACTORY
 * Copyright(c) 2022 Don Nuwinda Udugala
 * CC Licensed
 */
import MysqlService from './lib/mysqldriver'

import cors from 'cors'
import express from 'express'

class LoadDrivers{
    private app: any;
    private server: any;
    private mysqlPool: any;

    constructor(props :any) {
        this.app = props;
        this.app.use(cors);
        this.app.use(express.json());
        this.app.use(express.urlencoded({extended : false}));
        this.mysqlPool = new MysqlService();
    }
}

export default LoadDrivers;