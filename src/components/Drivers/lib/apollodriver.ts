/*!
 * apollow drivers
 * Copyright(c) 2022 KOTAFACTORY
 * Copyright(c) 2022 Don Nuwinda Udugala
 * CC Licensed
 */
import { ApolloServer, gql }  from "apollo-server-express"
import {createConnection} from "mysql";

class ApolloDriver{
    private server: any | undefined = undefined;
    private app: any | undefined;
    private url: string | undefined = undefined;

    constructor(props: any) {
        this.app = props;
        console.log(props);
        this.startServer();
        return this.getApolloInsance();
    }

    startServer(){
        // Construct a schema, using GraphQL schema language
        const typeDefs = gql`
          type Query {
            hello: String
          }
        `;

        // Provide resolver functions for your schema fields
        const resolvers = {
            Query: {
                hello: () => 'Hello world!',
            },
        };

        this.apolloInstance(typeDefs, resolvers);
        this.listenApollo();
        this.getServerDetails();
        return this.getApolloInsance();
    }

    apolloInstance(typeDefs:any, resolvers:any){
        this.server = new ApolloServer({
            typeDefs,
            resolvers,
            csrfPrevention: true,
            cache: "bounded",
        });
    }

    listenApollo() {
        if (this.server==undefined) {
            this.server.start();
            this.url = this.server.listen(this.app);
        }
    }

    getApolloInsance() : any{
        return this.server;
    }

    getServerDetails(){
        console.log(this.server);
    }
}

export default ApolloDriver;