/*!
 * app
 * Copyright(c) 2022 KOTAFACTORY
 * Copyright(c) 2022 Don Nuwinda Udugala
 * CC Licensed
 */
import {createPool, Pool} from "mysql";

class Mysqldriver {
    private pool: any | undefined = undefined;
    private mysqldata: object = {};

    constructor() {
        this.connectMysql();
    }

    connectMysql(){
        try{
            // @ts-ignore
            this.pool = createPool({
                connectionLimit:30,
                host:process.env.MY_SQL_DB_HOST,
                user:process.env.MY_SQL_DB_USER,
                password:process.env.MY_SQL_DB_PASSWORD,
                database:process.env.MY_SQL_DB_DATABASE,
                port:18106,
                debug: true
            })
            console.debug('Pool Created');
        } catch (err:any) {
            //console.log(process.env);
            throw new Error(err);
        }
    }

    runQuery(sqlString:string, values:[]){
        if(!this.pool) this.connectMysql();

        return new Promise((resolve, reject) => {
            this.pool.query(sqlString, values, (err:any, results:any) => {
                    if(err) {
                        reject(err);
                        return;
                    }

                    resolve(results);
                }
            );
        });
    }

    getPool(){
        return this.pool;
    }
}

export default Mysqldriver;