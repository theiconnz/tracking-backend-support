/*!
 * middleware
 * Copyright(c) 2022 KOTAFACTORY
 * Copyright(c) 2022 Don Nuwinda Udugala
 * CC Licensed
 */
import ApolloDriver from '../Middleware'

class Middleware{
    private app:any;

    constructor(app:any) {
        this.app = app;
    }
}

export default Middleware;